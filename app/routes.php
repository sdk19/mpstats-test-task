<?php

declare(strict_types=1);

use App\Application\Actions\Wildberries\SearchItem;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;

return function (App $app) {
    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('<h1>Greetings "MPSTATS"!</h1>');
        return $response;
    });

    $app->get('/wildberries/search/{word}', SearchItem::class);
};
