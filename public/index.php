<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

(require __DIR__ . '/../app/routes.php')($app);

$app->addRoutingMiddleware();
$app->addErrorMiddleware(false, true, true);

$app->run();
