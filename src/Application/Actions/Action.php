<?php

declare(strict_types=1);

namespace App\Application\Actions;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpInternalServerErrorException;

abstract class Action
{
    protected Request $request;

    protected Response $response;

    protected array $args;

    abstract protected function action(): Response;

    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;

        return $this->action();
    }

    protected function resolveArg(string $name): mixed
    {
        if (!isset($this->args[$name])) {
            throw new HttpBadRequestException($this->request, "Could not resolve argument `{$name}`");
        }

        return $this->args[$name];
    }

    protected function respond(object|array $payload = null, int $statusCode = 200): Response
    {
        $json = json_encode($payload, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

        $this->response->getBody()->write($json);

        return $this->response->withHeader('Content-Type', 'application/json')
            ->withStatus($statusCode);
    }
}
