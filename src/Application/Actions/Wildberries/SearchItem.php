<?php

declare(strict_types=1);

namespace App\Application\Actions\Wildberries;

use App\Application\Actions\Action;
use App\Application\Handler\RedisClient;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;

class SearchItem extends Action
{
    protected function action(): Response
    {
        $searchWord = $this->resolveArg('word');

        $redis = RedisClient::getInstance()->getRedis();
        $products = $redis->hget('products', $searchWord);

        if (!$products) {
            throw new HttpNotFoundException($this->request, 'Nothing was found');
        }

        $products = json_decode($products, true);

        $products = array_map(function ($product) {
            return [
                'id' => $product['id'],
                'name' => $product['name'],
                'supplier' => $product['supplier'],
                'reviewRating' => $product['reviewRating'],
            ];
        }, $products);

        return $this->respond($products);
    }
}
