<?php

namespace App\Application\Console;

use App\Application\Handler\RedisClient;
use Exception;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ParseSearchCommand extends Command
{
    protected function configure(): void
    {
        parent::configure();

        $this->setName('wildberries:parse-search');
        $this->setDescription('Parse "Wildberries" search page');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $ui = new SymfonyStyle($input, $output);
        $ui = $ui->getErrorStyle();

        $ui->warning('Starting to parse API..');

        $client = new Client(['timeout' => 0, 'verify' => false]);
        $url = 'https://search.wb.ru/exactmatch/ru/common/v4/search';

        foreach ($this->getSearchWords() as $word) {
            $retry = 3;

            while ($retry > 0) {
                try {
                    $response = $client->get($url, [
                        'query' => $this->getSearchParams($word),
                        'headers' => $this->getRequestHeaders()
                    ]);

                    $response = json_decode($response->getBody(), true);

                    if (!isset($response['data']['products']) || count($response['data']['products']) < 100) {
                        throw new Exception('Bad response for products items');
                    }

                    $products = $response['data']['products'];

                    $redis = RedisClient::getInstance()->getRedis();
                    $redis->hset('products', $word, json_encode($products));

                    break;
                } catch (Exception $e) {
                    $retry--;
                    $ui->warning('Error while requesting API: '.$e->getMessage());

                    if ($retry == 0) {
                        $ui->error("Retries exhausted for API request: $url");
                    } else {
                        $ui->warning("Retrying API request: $url");
                    }

                    sleep(5);
                }
            }
        }

        $ui->success('Parse is done!');
        return Command::SUCCESS;
    }

    private function getRequestHeaders(): array
    {
        return [
            'Accept' => '*/*',
            'Referer' => 'https://www.wildberries.ru/catalog/0/search.aspx',
            'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36',
        ];
    }

    private function getSearchWords(): array
    {
        return [
            'джинсы',
            'платье',
            'футболка',
        ];
    }

    private function getSearchParams(string $query): array
    {
        // Параметры для запроса к API WB, выдраны путем реверс-инженеринга страницы с поиском.
        // Нашел публичное API https://openapi.wildberries.ru/,
        // Но как я понял, оно подходит только для получения информации, о собственных товарах селлера

        return [
            'ab_testing' => 'false',
            'appType' => 1,
            'curr' => 'rub', // Валюта
            'dest' => '-1257786', // Видимо идентификатор назначения (индекс Москвы)
            'query' => $query,
            'resultset' => 'catalog',
            'sort' => 'popular', // Сортировка по популярности
            'spp' => 30,
            'suppressSpellcheck' => 'false',
        ];
    }
}
