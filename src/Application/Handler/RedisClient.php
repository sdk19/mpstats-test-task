<?php

declare(strict_types=1);

namespace App\Application\Handler;

use Predis\Client;

final class RedisClient
{
    private static ?RedisClient $instance = null;

    private Client $redis;

    private function __construct()
    {
        $this->redis = new Client([
            'scheme' => 'tcp',
            'host' => getenv('DOCKER') ? 'redis' : 'localhost',
            'port' => 6379
        ]);
    }

    public static function getInstance(): RedisClient
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getRedis(): Client
    {
        return $this->redis;
    }
}
